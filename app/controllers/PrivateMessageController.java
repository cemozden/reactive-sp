package controllers;

import actors.AnswerActor;
import actors.PrivateMessageActor;
import actors.UserActor;
import actors.messages.*;
import akka.actor.ActorRef;
import akka.actor.Inbox;
import akka.actor.Props;
import models.Answer;
import models.User;
import services.MailService;
import services.serviceimpl.MailServiceImpl;
import views.html.viewpmessages;
import models.PrivateMessage;
import play.data.Form;
import play.libs.Akka;
import play.mvc.Controller;
import play.mvc.Result;
import scala.concurrent.duration.Duration;
import util.DateUtil;
import util.IDUtil;
import views.html.sendmessage;
import views.html.successful;
import views.html.viewpmessage;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static play.libs.F.Promise;

/**
 * PrivateMessage Controller class
 *
 * @author ozden
 * Date: 23rd of October.
 *
 * @version 1.0 MILESTONE 1.0
 *
 * Manages PrivateMessage actions provides utilities to add, read messages from the system..
 */
public class PrivateMessageController extends Controller {

    /**
     * sendMessage Action
     *
     * Lets user to send a private message to another users.
     * if the user hasn't logged in into the system, s/he's redirected to the login page.
     * */
    public Result sendMessage(String userId)
    {
        if (session("user") == null) return redirect("/login");
        return ok(sendmessage.render(userId));
    }

    /**
     * sendMessageProcess
     *
     * Gets a private message object saves it into the system.
     * @return a successful message if the save is successful.
     * */
    public Result sendMessageProcess()
    {
        final ActorRef pmActorRef = Akka.system().actorOf(Props.create(PrivateMessageActor.class));
        final ActorRef userActorRef = Akka.system().actorOf(Props.create(UserActor.class));

        final Inbox in = Inbox.create(Akka.system());
        final MailService ms = new MailServiceImpl();
        Form<PrivateMessage> userForm = Form.form(PrivateMessage.class);
        PrivateMessage pm = userForm.bindFromRequest().get();

        pm.setpMessageId(IDUtil.generateId(10));
        pm.setSender(session("userId"));
        pm.setSentDate(DateUtil.getSQLDate(new Date()));
        pm.setRead("N");

        in.send(pmActorRef, new SendPrivateMessageMessage(pm));
        String returnMessage = (String) in.receive(Duration.create(1, TimeUnit.SECONDS));

        in.send(userActorRef, new GetUserMessage(pm.getSender()));
        pm.setSenderObject((User) in.receive(Duration.create(1, TimeUnit.SECONDS)));

        in.send(userActorRef, new GetUserMessage(pm.getReceiver()));

        ms.sendEMail((User) in.receive(Duration.create(1, TimeUnit.SECONDS)), pm);



        return ok(successful.render(returnMessage));
    }
    /**
     * viewPrivateMessages Action
     *
     * Shows the private messages of the logged-in user.
     * */
    public Promise<Result> viewPrivateMessages() {
        final ActorRef pmActorRef = Akka.system().actorOf(Props.create(PrivateMessageActor.class));
        final Inbox in = Inbox.create(Akka.system());

        if (session("user") == null) return Promise.promise(() -> redirect("/"));

        in.send(pmActorRef, new GetPrivateMessages(session("userId")));

        Promise resultPromise = Promise.promise(() -> (List<PrivateMessage>) in.receive(Duration.create(1, TimeUnit.SECONDS))).
                map((List<PrivateMessage> privateMessages) -> ok(viewpmessages.render(privateMessages)));

        return resultPromise;
    }

    /**
     * viewPrivateMessage Action
     *
     * shows up a specific message of the logged-in user.
     * */
    public Promise<Result> viewPrivateMessage(String pMessageId) {
        final ActorRef pmActorRef = Akka.system().actorOf(Props.create(PrivateMessageActor.class));
        final Inbox in = Inbox.create(Akka.system());


        in.send(pmActorRef, new GetPrivateMessage(pMessageId));

        Promise resultPromise = Promise.promise(() -> (PrivateMessage) in.receive(Duration.create(1, TimeUnit.SECONDS))).
                map((PrivateMessage privateMessage) -> ok(viewpmessage.render(privateMessage)));

        return resultPromise;
    }

}
