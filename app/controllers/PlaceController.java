package controllers;

import actors.CityActor;
import actors.PlaceActor;
import actors.messages.AddPlaceMessage;
import actors.messages.AddPlaceTypeMessage;
import actors.messages.GetCityBySchoolIdMessage;
import actors.messages.GetPlaceTypesMessage;
import akka.actor.ActorRef;
import akka.actor.Inbox;
import akka.actor.Props;
import models.City;
import models.Place;
import models.PlaceType;
import play.Logger;
import play.data.Form;
import play.libs.Akka;
import play.libs.F;
import play.mvc.Controller;
import play.mvc.Result;
import scala.concurrent.duration.Duration;
import util.IDUtil;
import views.html.*;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static play.libs.F.Promise;

/**
 * PlaceController class
 *
 * @author ozden
 * Date: 21st of October.
 *
 * @version 1.0 MILESTONE 1.3
 *
 * Manages Place actions provides utilities to add, read places from the system..
 */
public class PlaceController extends Controller {

    /**
     * addPlaceType Action
     *
     * Main action of the place type in the controller.
     * Opens the add place type page as long as the user has logged-in
     * */
    public Result addPlaceType()
    {
        if (session("user") == null) return redirect("/login");
        return ok(addplacetype.render());
    }

    /**
     * addPlaceTypeProcess Action
     *
     * Gets the place type object saves it into the system.
     * @return successful message if the save is done.
     * */
    public Result addPlaceTypeProcess()
    {
        final ActorRef userActorRef = Akka.system().actorOf(Props.create(PlaceActor.class));
        final Inbox in = Inbox.create(Akka.system());

        Form<PlaceType> userForm = Form.form(PlaceType.class);

        PlaceType pt = userForm.bindFromRequest().get();
        pt.setPlaceTypeId(IDUtil.generateId(10));

        in.send(userActorRef, new AddPlaceTypeMessage(pt));
        String returnMessage = (String) in.receive(Duration.create(1, TimeUnit.SECONDS));

        Logger.info("A place type has been added. Type: " + pt.getType());

        return ok(successful.render(returnMessage));
    }

    /**
     * addPlace Action
     *
     * Main action of the addPlace in the controller.
     * Gets city name and place types from the system.
     * The user can only add a place where s/he's located.
     * */
    public Promise<Result> addPlace() {
        final ActorRef cityActorRef = Akka.system().actorOf(Props.create(CityActor.class));
        final ActorRef placeActorRef = Akka.system().actorOf(Props.create(PlaceActor.class));
        final Inbox in = Inbox.create(Akka.system());

        if(session("user") == null) return Promise.promise(() -> redirect("/login"));

        in.send(cityActorRef, new GetCityBySchoolIdMessage(session("schoolId")));
        City c = (City) in.receive(Duration.create(1, TimeUnit.SECONDS));

        in.send(placeActorRef, new GetPlaceTypesMessage());

        Promise<Result> resultPromise = Promise.promise(() -> (List<PlaceType>) in.receive(Duration.create(1, TimeUnit.SECONDS))).
                map((List<PlaceType> placeTypes) -> ok(addplace.render(c.getCityId(), c.getName(), placeTypes)));

        return resultPromise;
    }

    /**
     * addPlaceProcess Action
     *
     * Gets the place object and saves it into the system.
     * @return a successful message if the save is successful.
     * */
    public Result addPlaceProcess()
    {
        final ActorRef placeActorRef = Akka.system().actorOf(Props.create(PlaceActor.class));
        final Inbox in = Inbox.create(Akka.system());

        Form<Place> placeForm = Form.form(Place.class);
        Place p = placeForm.bindFromRequest().get();
        p.setPlaceId(IDUtil.generateId(10));

        in.send(placeActorRef, new AddPlaceMessage(p));

        Logger.info("The place \""+ p.getName() +"\" has been added.");

        return ok(successful.render((String) in.receive(Duration.create(1, TimeUnit.SECONDS))));
    }

}
