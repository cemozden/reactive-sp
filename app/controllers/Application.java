package controllers;

import actors.PlaceActor;
import actors.PrivateMessageActor;
import actors.QuestionActor;
import actors.SchoolActor;
import actors.messages.GetLastQuestionsMessage;
import actors.messages.GetNumberOfUnreadMessages;
import actors.messages.GetSchoolInformationMessage;
import actors.messages.SearchMessage;
import akka.actor.ActorRef;
import akka.actor.Inbox;
import akka.actor.Props;
import models.Place;
import models.Question;
import models.SchoolInformation;
import models.Search;
import play.*;
import play.data.Form;
import play.libs.Akka;
import play.mvc.*;

import scala.concurrent.duration.Duration;
import views.html.*;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Application class
 *
 * @author ozden
 * Date: 17th of October
 *
 * The main Controller of the program. The software starts from the index action of the Application Controller.
 * Shows up two common components,
 * Index that shows the latest questions and school information
 * Search that searches the questions and places.
 * */
public class Application extends Controller {

    /**
     * Index Action
     * The main action of the program
     *
     * Provides question list, school information list if the user has logged-in to the system.
     * It's the action when the program is loaded.
     * */
    public Result index() {
        final ActorRef questionActorRef = Akka.system().actorOf(Props.create(QuestionActor.class));
        final ActorRef schoolActorRef = Akka.system().actorOf(Props.create(SchoolActor.class));
        final ActorRef pMessageActorRef = Akka.system().actorOf(Props.create(PrivateMessageActor.class));

        final Inbox in = Inbox.create(Akka.system());

        List<SchoolInformation> siList = null;
        int numOfUnreadMessages = 0;

        in.send(questionActorRef, new GetLastQuestionsMessage(10));
        List<Question> qList = (List<Question>) in.receive(Duration.create(1, TimeUnit.SECONDS));

        // if the user is logged in, show up the school information of the user as well as the number of unread messages.
        if (session("user") != null)
        {
            in.send(schoolActorRef, new GetSchoolInformationMessage(session("schoolId")));
            siList = (List<SchoolInformation>) in.receive(Duration.create(1, TimeUnit.SECONDS));

            in.send(pMessageActorRef, new GetNumberOfUnreadMessages(session("userId")));
            numOfUnreadMessages = (int) in.receive(Duration.create(1, TimeUnit.SECONDS));
            Logger.info("Number of unread messages: " + numOfUnreadMessages);
        }

        return ok(index.render(qList, siList));
    }

    /**
     * searchResult Action
     * The POST action that is processed after the search is done.
     * */
    public Result searchResult()
    {
        final ActorRef questionActorRef = Akka.system().actorOf(Props.create(QuestionActor.class));
        final ActorRef placeActorRef = Akka.system().actorOf(Props.create(PlaceActor.class));
        final Inbox in = Inbox.create(Akka.system());

        Form<Search> searchForm = Form.form(Search.class);

        Search s = searchForm.bindFromRequest().get();

        in.send(questionActorRef, new SearchMessage(s.getSearch()));
        List<Question> qList = (List<Question>) in.receive(Duration.create(1, TimeUnit.SECONDS));

        in.send(placeActorRef, new SearchMessage(s.getSearch()));
        List<Place> pList = (List<Place>) in.receive(Duration.create(1, TimeUnit.SECONDS));

        return ok(search.render(s.getSearch(), qList, pList));

    }

}
