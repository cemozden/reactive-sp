package controllers;

import actors.SchoolActor;
import actors.messages.AddSchoolInformationMessage;
import actors.messages.GetSchoolsByCityMessage;
import akka.actor.ActorRef;
import akka.actor.Inbox;
import akka.actor.Props;
import models.School;
import models.SchoolInformation;
import play.data.Form;
import play.libs.Akka;
import static play.libs.F.Promise;
import play.mvc.Controller;
import play.mvc.Result;
import scala.concurrent.duration.Duration;
import util.IDUtil;
import views.html.addsi;
import views.html.successful;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * School Controller class
 *
 * @author ozden
 * Date: 23rd of October.
 *
 * @version 1.0 MILESTONE 1.0
 *
 * Manages School actions provides utilities to add, read schools from the system..
 */
public class SchoolController extends Controller {

    /**
     * addSchoolInformation Action
     *
     * shows up the adding school information page.
     * Informations about the school can be added through using this action.
     * */
    public Result addSchoolInformation()
    {
        if (session("user") == null) return redirect("/login");
        return ok(addsi.render());
    }

    /**
     * addSchoolInformationProcess Action
     *
     * Adds the given School Information object into the system
     * @return a successful message if the save is successful.
     * */
    public Result addSchoolInformationProcess()
    {
        final ActorRef schoolActorRef = Akka.system().actorOf(Props.create(SchoolActor.class));
        final Inbox in = Inbox.create(Akka.system());

        Form<SchoolInformation> userForm = Form.form(SchoolInformation.class);
        SchoolInformation si = userForm.bindFromRequest().get();

        si.setSiid(IDUtil.generateId(10));
        si.setSchoolId(session("schoolId"));

        in.send(schoolActorRef, new AddSchoolInformationMessage(si));

        String returnMessage = (String) in.receive(Duration.create(1, TimeUnit.SECONDS));

        return ok(successful.render(returnMessage));
    }

    /**
     * getSchoolsAJAX Action
     *
     * An AJAX POST Action which gets a school id from the form
     * @return gives back a list of schools which belong to a specific city.
     *
     * */
    public Promise<Result> getSchoolsAJAX(String schoolId) {
        final ActorRef schoolActorRef = Akka.system().actorOf(Props.create(SchoolActor.class));
        final Inbox in = Inbox.create(Akka.system());

        in.send(schoolActorRef, new GetSchoolsByCityMessage(schoolId));

        Promise<Result> resultPromise = Promise.promise(() -> (List<School>) in.receive(Duration.create(1, TimeUnit.SECONDS))).
                map((List<School> schools) -> {

                    response().setContentType("text/html");
                    StringBuffer sb = new StringBuffer();

                    for (School s : schools) {
                        sb.append("<option value=\"").append(s.getSchoolId()).append("\">");
                        sb.append(s.getName()).append("</option>");
                    }

                    return ok(sb.toString());
                });

        return resultPromise;
    }

}
