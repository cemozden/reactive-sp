package controllers;

import actors.AnswerActor;
import actors.QuestionActor;
import actors.messages.AddQuestionMessage;
import actors.messages.GetAllQuestionsMessage;
import actors.messages.GetAnswersOfQuestionMessage;
import actors.messages.GetQuestionMessage;
import akka.actor.ActorRef;
import akka.actor.Inbox;
import akka.actor.Props;
import models.Answer;
import models.Question;
import play.data.Form;
import play.libs.Akka;
import play.mvc.Controller;
import play.mvc.Result;
import scala.concurrent.duration.Duration;
import util.DateUtil;
import util.IDUtil;
import views.html.addquestion;
import views.html.successful;
import views.html.questions;
import views.html.question;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;


/**
 * Question Controller class
 *
 * @author ozden
 * Date: 22nd of October.
 *
 * @version 1.0 MILESTONE 1.1
 *
 * Manages Question actions provides utilities to add, read questions from the system..
 */
public class QuestionController extends Controller {

    /**
     * addQuestion Action
     *
     * shows the page where users can add new questions.
     * @return a view page of adding question.
     * */
    public Result addQuestion()
    {
        if (session("user") == null) return redirect("/login");
        return ok(addquestion.render());
    }

    /**
     * addQuestionProcess Action
     *
     * The action that deals with adding a new question.
     * @return a successful message if adding is done.
     * */
    public Result addQuestionProcess()
    {
        final ActorRef questionActorRef = Akka.system().actorOf(Props.create(QuestionActor.class));
        final Inbox in = Inbox.create(Akka.system());

        Form<Question> userForm = Form.form(Question.class);
        Question q = userForm.bindFromRequest().get();
        
        q.setQuestionId(IDUtil.generateId(10));
        q.setSender(session("userId"));
        q.setSentDate(DateUtil.getSQLDate(new Date()));
        in.send(questionActorRef, new AddQuestionMessage(q));

        String returnMessage = (String) in.receive(Duration.create(1, TimeUnit.SECONDS));

        return redirect("/question/" + q.getQuestionId());
    }

    /**
     * questions Action
     *
     * The action that shows up the questions that are added before.
     * */
    public Result questions()
    {
        final ActorRef questionActorRef = Akka.system().actorOf(Props.create(QuestionActor.class));
        final Inbox in = Inbox.create(Akka.system());

        in.send(questionActorRef, new GetAllQuestionsMessage());
        List<Question> qList = (List<Question>) in.receive(Duration.create(1, TimeUnit.SECONDS));

        return ok(questions.render(qList));
    }

    /**
     * showQuestion Action
     *
     * The action that shows a specific question which is sent by a GET parameter.
     * */
    public Result showQuestion(String questionId)
    {
        final ActorRef questionActorRef = Akka.system().actorOf(Props.create(QuestionActor.class));
        final ActorRef answerActorRef = Akka.system().actorOf(Props.create(AnswerActor.class));
        final Inbox in = Inbox.create(Akka.system());

        in.send(questionActorRef, new GetQuestionMessage(questionId));
        Question q = (Question) in.receive(Duration.create(1, TimeUnit.SECONDS));

        in.send(answerActorRef, new GetAnswersOfQuestionMessage(questionId));
        List<Answer> answerList = (List<Answer>) in.receive(Duration.create(1, TimeUnit.SECONDS));

        return ok(question.render(q, answerList));
    }

}
