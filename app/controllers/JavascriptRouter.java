package controllers;

import play.Routes;
import play.mvc.Controller;
import play.mvc.Result;
/**
 * JavascriptRouter class
 *
 * @author ozden
 * Date: 21st of October.
 *
 * Manages AJAX requests from the client-side app.
 * Giving url definitions for the javascript calls.
 */
public class JavascriptRouter extends Controller {

    public Result javascriptRoutes()
    {
        response().setContentType("text/javascript");
        return ok(Routes.javascriptRouter("jsRoutes",
                routes.javascript.UserController.getCitiesAJAX(),
                routes.javascript.SchoolController.getSchoolsAJAX()

        ));
    }

}
