package controllers;

import actors.CityActor;
import actors.QuestionActor;
import actors.UserActor;
import actors.messages.*;
import akka.actor.ActorRef;
import akka.actor.Inbox;
import akka.actor.Props;
import models.*;
import play.Logger;
import play.data.Form;
import play.libs.Akka;
import play.mvc.*;
import scala.concurrent.duration.Duration;
import util.IDUtil;
import views.html.*;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static play.libs.F.Promise;

/**
 * UserController class
 *
 * @author ozden
 * Date: 15th of October.
 *
 * Manages User actions provides AJAX solutions for the user registration.
 * Registration request goes into the addUser action and saved into the database.
 */

public class UserController extends Controller {

    /**
     * Index Action
     *
     * Main action in the controller.
     * Deals with providing country list into the registration form
     * Used Promises to deal asynchronous streaming while JPA operations are being handled.
     *
     * */
    public Promise<Result> index() {
        final ActorRef userActorRef = Akka.system().actorOf(Props.create(UserActor.class));
        final ActorRef questionActorRef = Akka.system().actorOf(Props.create(QuestionActor.class));
        final Inbox in = Inbox.create(Akka.system());

        if(session("user") != null) return Promise.promise(() ->redirect("/"));


        // Send the GetCountriesMessage to get country list.
        in.send(userActorRef, new GetCountriesMessage());

        Promise<Result> resultPromise = Promise.promise(() ->
                (List<Country>) in.receive(Duration.create(1, TimeUnit.SECONDS))).map((List<Country> countries) ->
                    ok(useregister.render(countries)));


        return resultPromise;
    }

    /**
     * addUser Action
     *
     * POST Action which is activated by the index action to save the user
     * A session is created after the registration is completed successfully.
     * */
    public Result addUser()
    {
        final ActorRef userActorRef = Akka.system().actorOf(Props.create(UserActor.class));
        final Inbox in = Inbox.create(Akka.system());

        Form<User> userForm = Form.form(User.class);

        User u = userForm.bindFromRequest().get();
        u.setUserId(IDUtil.generateId(10));

        in.send(userActorRef, new UserRegisterMessage(u));

        Logger.info("User has been added. Username: " + u.getUsername());

        return ok(successful.render((String) in.receive(Duration.create(1, TimeUnit.SECONDS))));
    }

    /**
     * login Action
     *
     * GET Action which is activated when a user wants to log in into the system.
     * If a user is already logged-in then the user is redirected to the index page.
     * */
    public Result login()
    {
        final ActorRef questionActorRef = Akka.system().actorOf(Props.create(QuestionActor.class));
        final Inbox in = Inbox.create(Akka.system());

        if (session("user") != null) return redirect("/");

        return ok(login.render(""));
    }

    /**
     * loginProcess Action
     *
     * POST Action which is activated by the login action.
     * If the username and password match with a user's data then the username, school id of the user as well as the user id
     * is saved into the session and redirected into the index page.
     *
     * */
    public Result loginProcess()
    {
        final ActorRef userActorRef = Akka.system().actorOf(Props.create(UserActor.class));
        final Inbox in = Inbox.create(Akka.system());

        Form<User> userForm = Form.form(User.class);
        User u = userForm.bindFromRequest().get();

        // send a message to check whether the the username and password match.
        in.send(userActorRef, new CheckUserMessage(u));

        boolean isUser = (boolean) in.receive(Duration.create(1, TimeUnit.SECONDS));

        if (isUser)
        {
            session("user", u.getUsername());
            session("schoolId", u.getSchoolId());
            session("userId", u.getUserId());

            Logger.info("The user \""+session("user")+"\" has been logged-in!");

            return redirect("/");
        }
        else return ok(login.render("The username or password is wrong! Please check your data!"));

    }

    /**
     * logout Action
     *
     * GET Action which is activated by the logged-in user.
     * If a user hasn't been logged in into the system then the user is redirected to the index page.
     * If logout is successful then sessions are empty.
     * */
    public Result logout()
    {
        // if the user is logged in then make it unlogged.
        if (session("user") != null)
        {
            session().remove("user");
            session().remove("schoolId");
            session().remove("userId");
        }

        return redirect("/");
    }

    /**
     * getCitiesAJAX Action
     *
     * An AJAX POST Action that gets a country id from the form
     * @return a list of cities which belong to a specific country.
     * */
    public Promise<Result> getCitiesAJAX(String countryId) {
        final ActorRef cityActorRef = Akka.system().actorOf(Props.create(CityActor.class));
        final Inbox in = Inbox.create(Akka.system());

        in.send(cityActorRef, new GetCitiesByCountryNameMessage(countryId));

        Promise<Result> resultPromise = Promise.promise(() ->
                (List<City>) in.receive(Duration.create(1, TimeUnit.SECONDS))).map((List<City> cities) -> {
            response().setContentType("text/html");
            StringBuffer sb = new StringBuffer();

            // Create the options for the dropdown list in the registration form.
            for (City c : cities) {
                sb.append("<option value=\"").append(c.getCityId()).append("\">");
                sb.append(c.getName()).append("</option>");
            }

            return ok(sb.toString());

        });

        return resultPromise;
    }

    public Promise<Result> users()
    {
        final ActorRef userActorRef = Akka.system().actorOf(Props.create(UserActor.class));
        final Inbox in = Inbox.create(Akka.system());

        in.send(userActorRef, new UsersListMessage());

        Promise<Result> resultPromise = Promise.promise(() ->
                (List<User>) in.receive(Duration.create(1, TimeUnit.SECONDS))).map((List<User> userList) -> ok(users.render(userList)));


       return resultPromise;
    }

}

