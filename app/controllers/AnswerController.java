package controllers;

import actors.AnswerActor;
import actors.messages.AddAnswerMessage;
import akka.actor.ActorRef;
import akka.actor.Inbox;
import akka.actor.Props;
import models.Answer;
import play.data.Form;
import play.libs.Akka;
import play.mvc.Controller;
import play.mvc.Result;
import scala.concurrent.duration.Duration;
import util.DateUtil;
import util.IDUtil;
import views.html.successful;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * AnswerController class
 *
 * @author ozden
 * Date: 22nd of October
 *
 * Manages to add new answers to the system
 */
public class AnswerController extends Controller {

    /**
     * addAnswerProcess Action
     *
     * POST action that saves an answer for the specific question.
     * @return a message that specifies a successful message.
     * */
    public Result addAnswerProcess()
    {
        final ActorRef answerActorRef = Akka.system().actorOf(Props.create(AnswerActor.class));
        final Inbox in = Inbox.create(Akka.system());

        Form<Answer> userForm = Form.form(Answer.class);
        Answer answer = userForm.bindFromRequest().get();

        answer.setAnswerId(IDUtil.generateId(10));
        answer.setSender(session("userId"));
        answer.setSentDate(DateUtil.getSQLDate(new Date()));

        in.send(answerActorRef, new AddAnswerMessage(answer));

        String returnMessage = (String) in.receive(Duration.create(1, TimeUnit.SECONDS));

        return redirect("/question/" + answer.getQuestionId());
    }

}
