package services;

import models.School;
import models.SchoolInformation;
import org.w3c.dom.stylesheets.LinkStyle;

import java.util.List;

/**
 * Created by ozden on 10/21/15.
 */
public interface SchoolService {

    List<School> getSchoolsByCityName(String cityId);
    void addSchoolInformation(SchoolInformation si);
    List<SchoolInformation> getSchoolInformationList(String schoolId);
}
