package services;

import models.Place;
import models.PlaceType;

import java.util.List;

/**
 * Created by ozden on 10/21/15.
 */
public interface PlaceService {

    void addPlaceType(PlaceType pt);
    void addPlace(Place p);
    List<PlaceType> getPlaceTypes();
    List<Place> getSearchedPlaces(String word);

}
