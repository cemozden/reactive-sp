package services.serviceimpl;

import models.City;
import models.Country;
import play.db.jpa.JPA;
import play.libs.F;
import services.CityService;

import javax.persistence.Query;
import java.util.List;

/**
 * City Service Implementation Class
 * @author ozden
 * Date: 19th of October.
 * @version 1.0 MILESTONE 2
 *
 * The responsible implementation to save and get the cities.
 */
public class CityServiceImpl implements CityService {

    @Override
    public City getCityBySchoolId(String schoolId)
    {
        List<City> cityList = null;
        try {
            cityList = JPA.withTransaction(() -> {
                Query qry = JPA.em().createQuery("SELECT ci FROM City ci JOIN ci.schools sc WHERE sc.schoolId = :id", City.class);
                qry.setParameter("id", schoolId);

                return qry.getResultList();
            });
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        return cityList.get(0);
    }

    @Override
    public List<City> getCitiesByCountryName(String countryId) {

        List<City> cityList = null;
        try {
            cityList = JPA.withTransaction(() -> {
                Query qry = JPA.em().createQuery("SELECT ci FROM Country co JOIN co.cities ci WHERE co.countryId = :id ORDER BY" +
                        " ci.name ASC", City.class);
                qry.setParameter("id", countryId);

                return qry.getResultList();
            });
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        return cityList;

    }
}
