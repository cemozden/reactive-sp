package services.serviceimpl;

import models.Question;
import models.User;
import play.db.jpa.JPA;
import play.libs.F;
import services.QuestionService;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * Question Service Implementation Class
 * @author ozden
 * Date: 22nd of October.
 * @version 1.0 MILESTONE 1.2
 *
 * The responsible implementation to save and get the questions.
 */

public class QuestionServiceImpl implements QuestionService {
    @Override
    public void addQuestion(Question q) { JPA.withTransaction(() -> JPA.em().persist(q)); }

    @Override
    public List<Question> getLastQuestions(int limit) {
        List<Question> questionList = null;
        try {
            questionList = JPA.withTransaction(() -> {
                Query qry = JPA.em().createQuery("SELECT q, u FROM User u JOIN u.questions q ORDER BY q.sentDate DESC");
                List<Question> qList = new ArrayList<Question>();
                qry.setMaxResults(limit);
                List<Object[]> tables = qry.getResultList();

                for (Object[] oa : tables)
                {
                    Question q = (Question) oa[0];
                    q.setSenderObject((User) oa[1]);

                    qList.add(q);
                }


                return qList;
            });
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return questionList;
    }

    @Override
    public List<Question> getSearchedQuestions(String word) {
        List<Question> questionList = null;
        try {
            questionList = JPA.withTransaction(() -> {
                Query qry = JPA.em().createQuery("SELECT q, u FROM User u JOIN u.questions q WHERE q.title LIKE :w " +
                        "OR q.message LIKE :w ORDER BY q.sentDate DESC");
                List<Question> qList = new ArrayList<Question>();
                qry.setParameter("w", "%"+word+"%");

                List<Object[]> tables = qry.getResultList();

                for (Object[] oa : tables)
                {
                    Question q = (Question) oa[0];
                    q.setSenderObject((User) oa[1]);

                    qList.add(q);
                }


                return qList;
            });
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return questionList;
    }

    @Override
    public List<Question> getAllQuestions() {
        List<Question> questionList = null;
        try {
            questionList = JPA.withTransaction(() -> {
                Query qry = JPA.em().createQuery("SELECT q, u FROM User u JOIN u.questions q ORDER BY q.sentDate DESC");
                List<Question> qList = new ArrayList<Question>();
                List<Object[]> tables = qry.getResultList();

                for (Object[] oa : tables)
                {
                    Question q = (Question) oa[0];
                    q.setSenderObject((User) oa[1]);

                    qList.add(q);
                }


                return qList;
            });
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return questionList;
    }

    @Override
    public Question getQuestion(String questionId) {
        Question question = null;
        try {
            question = JPA.withTransaction(() -> {
                Query qry = JPA.em().createQuery("SELECT q, u FROM User u JOIN u.questions q WHERE q.questionId = :id");
                qry.setParameter("id", questionId);

                List<Question> qList = new ArrayList<Question>();
                List<Object[]> tables = qry.getResultList();

                for (Object[] oa : tables)
                {
                    Question q = (Question) oa[0];
                    q.setSenderObject((User) oa[1]);

                    qList.add(q);
                }


                return qList.get(0);
            });
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return question;
    }
}
