package services.serviceimpl;

import models.City;
import models.School;
import models.SchoolInformation;
import play.db.jpa.JPA;
import play.libs.F;
import services.SchoolService;

import javax.persistence.Query;
import java.util.List;

/**
 * Question Service Implementation Class
 * @author ozden
 * Date: 21st of October.
 * @version 1.0 MILESTONE 1
 *
 * The responsible implementation to save and get the schools as well as school information.
 */

public class SchoolServiceImpl implements SchoolService {


    @Override
    public List<School> getSchoolsByCityName(String cityId) {

        List<School> schoolList = null;
        try {
            schoolList = JPA.withTransaction(() -> {
                Query qry = JPA.em().createQuery("SELECT sc FROM City ci JOIN ci.schools sc WHERE ci.cityId = :id ORDER BY sc.name ASC", School.class);
                qry.setParameter("id", cityId);

                return qry.getResultList();
            });
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        return schoolList;
    }

    @Override
    public void addSchoolInformation(SchoolInformation si) {
        JPA.withTransaction(() -> JPA.em().persist(si));
    }

    @Override
    public List<SchoolInformation> getSchoolInformationList(String schoolId) {

        List<SchoolInformation> schoolInfList = null;
        try {
            schoolInfList = JPA.withTransaction(() -> {
                Query qry = JPA.em().createQuery("SELECT si FROM SchoolInformation si WHERE si.schoolId = :id ", SchoolInformation.class);
                qry.setParameter("id", schoolId);

                return qry.getResultList();
            });
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        return schoolInfList;

    }
}
