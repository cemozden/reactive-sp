package services.serviceimpl;

import models.Place;
import models.PlaceType;
import play.db.jpa.JPA;
import play.libs.F;
import services.PlaceService;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * Place Service Implementation Class
 * @author ozden
 * Date: 21st of October.
 * @version 1.0 MILESTONE 2
 *
 * The responsible implementation to save and get the places as well as place types.
 */
public class PlaceServiceImpl implements PlaceService {

    @Override
    public void addPlaceType(PlaceType pt) {
        JPA.withTransaction(() -> JPA.em().persist(pt));
    }

    @Override
    public void addPlace(Place p) {
        JPA.withTransaction(() ->JPA.em().persist(p));
    }

    @Override
    public List<PlaceType> getPlaceTypes() {

        List<PlaceType> placeTypeList = null;
        try {
            placeTypeList = JPA.withTransaction(() ->
                    JPA.em().createQuery("SELECT pt FROM PlaceType pt ORDER BY pt.type", PlaceType.class).getResultList());
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        return placeTypeList;
    }

    @Override
    public List<Place> getSearchedPlaces(String word) {
        List<Place> placeList = null;
        try {
            placeList = JPA.withTransaction(() -> {
                Query qry = JPA.em().createQuery("SELECT p, pt FROM Place p JOIN p.placeType pt WHERE p.name LIKE :w");

                qry.setParameter("w","%"+word+"%");

                List<Place> pList = new ArrayList<Place>();
                List<Object[]> tables = qry.getResultList();

                for (Object[] oa : tables)
                {
                    Place p = (Place) oa[0];
                    p.setPlaceType(((PlaceType) oa[1]));

                    pList.add(p);
                }

                return pList;
            });
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return placeList;
    }
}
