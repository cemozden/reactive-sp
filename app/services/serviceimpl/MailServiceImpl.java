package services.serviceimpl;

import models.PrivateMessage;
import models.User;
import play.Logger;
import services.MailService;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

/**
 * Mail Service Implementation Class
 * @author ozden
 * Date: 21st of October.
 * @version 1.0 MILESTONE 2
 *
 * The responsible implementation to send an email to the users.
 */
public class MailServiceImpl implements MailService {

    private final String sender = "reactivesp@gmail.com";
    private final String password = "reactivethesis";
    private final Session session;

    public MailServiceImpl()
    {
        Properties properties = System.getProperties();

        // Setup mail server
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.port", "587");

        // Get the default Session object.
        session = Session.getInstance(properties,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(sender, password);
                    }
                });
    }

    @Override
    public void sendEMail(User user, PrivateMessage pm) {

        StringBuffer sb = new StringBuffer();
        User sender = pm.getSenderObject();

        sb.append("You've got a new message from ").append(sender.getUsername()).append(" (").append(sender.getFirstname()).
                append(" ").append(sender.getLastname()).append(") \n\n");
        sb.append(pm.getTitle()).append("\n");
        sb.append(pm.getMessage());

        try{

            MimeMessage message = new MimeMessage(session);

            message.setFrom(new InternetAddress(this.sender, "Reactive Student Portal"));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(user.getEmail()));
            message.setSubject("Hey " + user.getFirstname() + "! You've got a new private message!");
            message.setText(sb.toString());

            Transport.send(message);
            Logger.info("Mail has been sent to " + user.getEmail() + " successfully!");
        }catch (MessagingException mex) {
            mex.printStackTrace();
        } catch (UnsupportedEncodingException ex) {
            ex.printStackTrace();
        }
    }
}
