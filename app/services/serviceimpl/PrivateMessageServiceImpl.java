package services.serviceimpl;

import models.City;
import models.PrivateMessage;
import models.User;
import play.db.jpa.JPA;
import play.libs.F;
import services.PrivateMessageService;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * Place Service Implementation Class
 * @author ozden
 * Date: 23rd of October.
 * @version 1.0 MILESTONE 2
 *
 * The responsible implementation to save and get the private messages.
 */
public class PrivateMessageServiceImpl implements PrivateMessageService {
    @Override
    public void sendPrivateMessage(PrivateMessage pm) {
        JPA.withTransaction(() -> JPA.em().persist(pm));
    }

    @Override
    public int getUnreadMessageCount(String receiver) {
        List<PrivateMessage> messageList = null;
        try {
            messageList = JPA.withTransaction(() -> {
                Query qry = JPA.em().createQuery("SELECT m FROM PrivateMessage m WHERE receiver=:r AND read='N'", PrivateMessage.class);
                qry.setParameter("r", receiver);

                return qry.getResultList();
            });
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        return messageList.size();
    }


    @Override
    public List<PrivateMessage> getPrivateMessages(String receiver) {
        List<PrivateMessage> messageList = null;
        try {
            messageList = JPA.withTransaction(() -> {
                Query qry = JPA.em().createQuery("SELECT m, u FROM User u JOIN u.privateMessages m WHERE m.receiver=:r " +
                        "ORDER BY m.sentDate DESC");
                qry.setParameter("r", receiver);

                List<PrivateMessage> qList = new ArrayList<PrivateMessage>();
                List<Object[]> tables = qry.getResultList();

                for (Object[] oa : tables)
                {
                    PrivateMessage pm = (PrivateMessage) oa[0];
                    pm.setSenderObject((User) oa[1]);

                    qList.add(pm);
                }

                return qList;
            });
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return messageList;
    }

    @Override
    public PrivateMessage getPrivateMessage(String pMessageId) {
        PrivateMessage message = null;
        try {
            message = JPA.withTransaction(() -> {
                Query qry = JPA.em().createQuery("SELECT m, u FROM User u JOIN u.privateMessages m WHERE m.pMessageId = :id");
                qry.setParameter("id", pMessageId);

                List<PrivateMessage> qList = new ArrayList<PrivateMessage>();
                List<Object[]> tables = qry.getResultList();

                for (Object[] oa : tables)
                {
                    PrivateMessage pm = (PrivateMessage) oa[0];
                    pm.setSenderObject((User) oa[1]);

                    qList.add(pm);
                }

                PrivateMessage pm = qList.get(0);

                pm.setRead("Y");

                JPA.em().flush();
                //JPA.em().getTransaction().commit();

                return pm;
            });
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        return message;
    }
}
