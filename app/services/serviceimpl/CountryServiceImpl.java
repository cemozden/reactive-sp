package services.serviceimpl;

import models.Country;
import play.db.jpa.JPA;
import play.libs.F;
import services.CountryService;

import java.util.List;

/**
 * Country Service Implementation Class
 * @author ozden
 * Date: 17th of October.
 * @version 1.0 MILESTONE 2
 *
 * The responsible implementation to get the countries.
 */
public class CountryServiceImpl implements CountryService {

    @Override
    public List<Country> getCountries() {
        List<Country> countryList = null;
        try {
            countryList = JPA.withTransaction(() ->
                    JPA.em().createQuery("SELECT c FROM Country c ORDER BY c.name", Country.class).getResultList());
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        return countryList;
    }
}
