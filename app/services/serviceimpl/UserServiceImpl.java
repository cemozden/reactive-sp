package services.serviceimpl;

import models.City;
import models.User;
import play.db.jpa.JPA;
import play.libs.F;
import services.UserService;

import javax.persistence.Query;
import java.util.List;

/**
 * User Service Implementation Class
 * @author ozden
 * Date: 17th of October.
 * @version 1.0 MILESTONE 1
 *
 * The responsible implementation to save and get the user information.
 */
public class UserServiceImpl implements UserService {


    @Override
    public void registerUser(User user) {
        JPA.withTransaction(() -> JPA.em().persist(user));
    }

    @Override
    public User getUser(String userId) {

        User u = null;

        try
        {
            u = JPA.withTransaction(() -> {
                Query qry = JPA.em().createQuery("SELECT u FROM User u WHERE u.userId = :userId", User.class);
                qry.setParameter("userId", userId);

                return (User) qry.getResultList().get(0);
            });
        }
        catch(Throwable t)
        {
            t.printStackTrace();
        }

        return u;
    }

    @Override
    public List<User> getUserList() {
        List<User> userList = null;

        try {
            userList = JPA.withTransaction(() -> {
                Query qry = JPA.em().createQuery("SELECT u FROM User u ORDER BY u.username", User.class);
                return qry.getResultList();
            });
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        return userList;
    }

    @Override
    public boolean isUser(User user) {

        List<User> userList = null;
        try {
            userList = JPA.withTransaction(() -> {
                Query qry = JPA.em().createQuery("SELECT u FROM User u WHERE u.username = :username AND " +
                        "u.password = :password", User.class);

                qry.setParameter("username", user.getUsername());
                qry.setParameter("password", user.getPassword());

                return qry.getResultList();
            });
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        if (userList.size() != 0)
        {
            user.setSchoolId(userList.get(0).getSchoolId());
            user.setUserId(userList.get(0).getUserId());
            return true;
        }
        else return false ;
    }
}
