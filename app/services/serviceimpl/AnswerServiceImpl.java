package services.serviceimpl;

import models.Answer;
import play.db.jpa.JPA;
import play.libs.F;
import services.AnswerService;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * Answer Service Implementation Class
 * @author ozden
 * Date: 22nd of October.
 * @version 1.0 MILESTONE 2
 *
 * The responsible implementation to save and get the answers of the questions.
 */

public class AnswerServiceImpl implements AnswerService {
    @Override
    public void addAnswer(Answer answer) {
        JPA.withTransaction(() -> JPA.em().persist(answer));
    }

    @Override
    public List<Answer> getAnswersOfQuestion(String questionId) {
        List<Answer> answerList = null;

        try {
            answerList = JPA.withTransaction(() ->
            {
                Query qry = JPA.em().createQuery("SELECT a FROM Answer a WHERE a.questionId = :id ORDER BY a.sentDate DESC");
                qry.setParameter("id", questionId);

                return qry.getResultList();
            });
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return answerList;
    }
}
