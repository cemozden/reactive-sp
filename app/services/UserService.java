package services;

import models.User;

import java.util.List;

/**
 * Created by ozden on 10/17/15.
 */
public interface UserService {

    void registerUser(User user);
    boolean isUser(User user);
    User getUser(String userId);
    List<User> getUserList();
}
