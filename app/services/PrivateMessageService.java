package services;

import models.PrivateMessage;

import java.util.List;

/**
 * Created by ozden on 10/23/15.
 */
public interface PrivateMessageService {

    void sendPrivateMessage(PrivateMessage pm);
    int getUnreadMessageCount(String receiver);
    List<PrivateMessage> getPrivateMessages(String receiver);
    PrivateMessage getPrivateMessage(String pMessageId);

}
