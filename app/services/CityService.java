package services;

import models.City;

import java.util.List;

/**
 * Created by ozden on 10/19/15.
 */
public interface CityService {

    List<City> getCitiesByCountryName(String countryId);
    City getCityBySchoolId(String schoolId);
}
