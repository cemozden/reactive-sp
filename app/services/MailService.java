package services;

import models.PrivateMessage;
import models.User;

/**
 * Created by ozden on 11/22/15.
 */
public interface MailService {

    void sendEMail(User user, PrivateMessage pm);

}
