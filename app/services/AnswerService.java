package services;

import models.Answer;

import java.util.List;

/**
 * Created by ozden on 10/22/15.
 */
public interface AnswerService {

    void addAnswer(Answer answer);
    List<Answer> getAnswersOfQuestion(String questionId);

}
