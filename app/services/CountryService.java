package services;

import models.Country;

import java.util.List;

/**
 * Created by ozden on 10/17/15.
 */
public interface CountryService {

    List<Country> getCountries();

}
