package services;

import models.Question;

import java.util.List;

/**
 * Created by ozden on 10/22/15.
 */
public interface QuestionService {

    void addQuestion(Question q);
    List<Question> getLastQuestions(int limit);
    List<Question> getSearchedQuestions(String word);
    List<Question> getAllQuestions();
    Question getQuestion(String questionId);
}
