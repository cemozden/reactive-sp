import actors.UserActor;
import actors.messages.GetCountriesMessage;
import akka.actor.ActorRef;
import akka.actor.Props;
import play.Application;
import play.GlobalSettings;
import play.Logger;
import play.libs.Akka;
import scala.concurrent.duration.Duration;

import java.util.concurrent.TimeUnit;

/**
 * Created by ozden on 10/29/15.
 */
public class Global extends GlobalSettings {

    @Override
    public void onStart(Application application) {
        final ActorRef userActorRef = Akka.system().actorOf(Props.create(UserActor.class));
        Logger.info("The application has started.");

       /* Akka.system().scheduler().schedule(Duration.Zero(),
                Duration.create(5, TimeUnit.SECONDS),userActorRef, new GetCountriesMessage(), Akka.system().dispatcher(), null);
*/
    }

    @Override
    public void onStop(Application application) {
        Logger.info("The application has stopped.");
    }
}
