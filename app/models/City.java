package models;

import javax.persistence.*;
import java.util.List;

/**
 * Created by ozden on 10/12/15.
 */

@Entity
@Table(name = "CITIES")
public class City
{
    public City(String cityId, String countryId, String name) {
        this.cityId = cityId;
        this.name = name;
        this.countryId = countryId;
    }

    public City() {}

    @Id
    @Column(name = "CITYID")
    private String cityId;

    @Column(name = "countryId")
    private String countryId;

    @Column(name = "NAME")
    private String name;

    @OneToMany(mappedBy = "cityId")
    private List<School> schools;

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String country) {
        this.countryId = countryId;
    }

    public List<School> getSchools() {
        return schools;
    }

    public void setSchools(List<School> schools) {
        this.schools = schools;
    }
}
