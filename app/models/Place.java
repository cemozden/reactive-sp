package models;

import javax.persistence.*;
import java.util.List;

/**
 * Created by ozden on 10/22/15.
 */
@Entity
@Table(name = "PLACES")
public class Place {

    @Id
    @Column(name = "PLACEID")
    private String placeId;

    @Column(name = "PLACETYPEID")
    private String placeTypeId;

    @Column(name = "NAME")
    private String name;

    @Column(name = "CITYID")
    private String cityId;

    @Column(name = "ADDRESS")
    private String address;

    @OneToOne(optional = false)
    @JoinColumn(name="PLACETYPEID", unique=true, insertable = false, nullable=false, updatable=false)
    private PlaceType placeType;

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPlaceTypeId() {
        return placeTypeId;
    }

    public void setPlaceTypeId(String placeTypeId) {
        this.placeTypeId = placeTypeId;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public PlaceType getPlaceType() {
        return placeType;
    }

    public void setPlaceType(PlaceType placeType) {
        this.placeType = placeType;
    }

    @Override
    public String toString() {
        return "Place{" +
                "placeId='" + placeId + '\'' +
                ", placeTypeId='" + placeTypeId + '\'' +
                ", name='" + name + '\'' +
                ", cityId='" + cityId + '\'' +
                ", address='" + address + '\'' +
                ", placeType=" + placeType +
                '}';
    }
}
