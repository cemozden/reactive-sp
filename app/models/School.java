package models;

import javax.persistence.*;

/**
 * Created by ozden on 10/21/15.
 */
@Entity
@Table(name = "SCHOOLS")
public class School {

    @Id
    @Column(name = "SCHOOLID")
    private String schoolId;

    @Column(name = "cityId")
    private String cityId;

    @Column(name = "NAME")
    private String name;

    @Column(name = "CAPACITY")
    private String capacity;

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }


    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }
}
