package models;

import javax.persistence.*;
import java.util.List;

/**
 * Created by ozden on 10/12/15.
 */
@Entity
@Table(name = "COUNTRIES")
public class Country
{
    public Country(String countryId, String name) {
        this.countryId = countryId;
        this.name = name;
    }

    public Country(){}

    @Id
    @Column(name = "COUNTRYID")
    private String countryId;

    @Column(name = "NAME")
    private String name;

    @OneToMany(mappedBy = "countryId")
    private List<City> cities;

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }
}
