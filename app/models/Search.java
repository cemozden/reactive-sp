package models;

/**
 * Created by ozden on 10/26/15.
 */
public class Search {

    private String search;

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }
}
