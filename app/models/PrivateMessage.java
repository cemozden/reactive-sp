package models;

import javax.persistence.*;
import java.sql.Date;

/**
 * Created by ozden on 10/23/15.
 */
@Entity
@Table(name = "PMESSAGES")
public class PrivateMessage {

    @Id
    @Column(name = "PMESSAGEID")
    private String pMessageId;

    @Column(name = "SENDER")
    private String sender;

    @Column(name = "RECEIVER")
    private String receiver;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "MESSAGE")
    private String message;

    @Column(name = "SENTDATE")
    private Date sentDate;

    @Column(name = "MESSAGEREAD")
    private String read;

    @OneToOne
    @JoinColumn(name = "SENDER", insertable = false, updatable = false)
    private User senderObject;

    public User getSenderObject() {
        return senderObject;
    }

    public void setSenderObject(User senderObject) {
        this.senderObject = senderObject;
    }

    public String getRead() {
        return read;
    }

    public void setRead(String read) {
        this.read = read;
    }

    public String getpMessageId() {
        return pMessageId;
    }

    public void setpMessageId(String pMessageId) {
        this.pMessageId = pMessageId;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getSentDate() {
        return sentDate;
    }

    public void setSentDate(Date sentDate) {
        this.sentDate = sentDate;
    }
}
