package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Date;

/**
 * Created by ozden on 10/22/15.
 */
@Entity
@Table(name = "ANSWERS")
public class Answer {

    @Id
    @Column(name = "ANSWERID")
    private String answerId;

    @Column(name = "QUESTIONID")
    private String questionId;

    @Column(name = "SENDER")
    private String sender;

    @Column(name = "SENTDATE")
    private Date sentDate;

    @Column(name = "MESSAGE")
    private String message;

    public String getAnswerId() {
        return answerId;
    }

    public void setAnswerId(String answerId) {
        this.answerId = answerId;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public Date getSentDate() {
        return sentDate;
    }

    public void setSentDate(Date sentDate) {
        this.sentDate = sentDate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }
}
