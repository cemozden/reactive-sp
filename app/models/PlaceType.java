package models;

import javax.persistence.*;

/**
 * Created by ozden on 10/21/15.
 */
@Entity
@Table(name = "PLACETYPE")
public class PlaceType {

    @Id
    @Column(name = "PLACETYPEID", unique=true, nullable=false, updatable=false)
    private String placeTypeId;

    @Column(name = "TYPE")
    private String type;

    @Column(name = "TYPECOMMENT")
    private String typeComment;

    @OneToOne(optional = false, mappedBy = "placeType")
    private Place place;

    public String getPlaceTypeId() {
        return placeTypeId;
    }

    public void setPlaceTypeId(String placeTypeId) {
        this.placeTypeId = placeTypeId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeComment() {
        return typeComment;
    }

    public void setTypeComment(String typeComment) {
        this.typeComment = typeComment;
    }

    @Override
    public String toString() {
        return "PlaceType{" +
                "placeTypeId='" + placeTypeId + '\'' +
                ", type='" + type + '\'' +
                ", typeComment='" + typeComment + '\'' +
                ", place=" + place +
                '}';
    }
}
