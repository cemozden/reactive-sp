package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by ozden on 10/23/15.
 */
@Entity
@Table(name = "SCHOOL_INFO")
public class SchoolInformation {

    @Id
    @Column(name = "SIID")
    private String siid;

    @Column(name = "SCHOOLID")
    private String schoolId;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "INFORMATION")
    private String information;

    public String getSiid() {
        return siid;
    }

    public void setSiid(String siid) {
        this.siid = siid;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }
}
