package models;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

/**
 * Created by ozden on 10/22/15.
 */
@Entity
@Table(name = "QUESTIONS")
public class Question {

    @Id
    @Column(name = "QUESTIONID")
    private String questionId;

    @Column(name = "SENDER")
    private String sender;

    @Column(name = "SENTDATE")
    private Date sentDate;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "MESSAGE")
    private String message;

    @OneToOne
    @JoinColumn(name = "SENDER", insertable = false, updatable = false)
    private User senderObject;

    @OneToMany(mappedBy = "questionId")
    private List<Answer> answerList;

    public String getQuestionId() {
        return questionId;
    }

    public User getSenderObject() {
        return senderObject;
    }

    public void setSenderObject(User senderObject) {
        this.senderObject = senderObject;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public Date getSentDate() {
        return sentDate;
    }

    public void setSentDate(Date sentDate) {
        this.sentDate = sentDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
