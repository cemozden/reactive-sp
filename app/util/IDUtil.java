package util;

import java.util.UUID;

/**
 * Created by ozden on 10/21/15.
 */
public class IDUtil {

    public static String generateId(int length)
    {
        String uuid = UUID.randomUUID().toString().replace("-", "");

        return uuid.substring(0, length);
    }
}
