package util;

import java.sql.Date;
import java.text.SimpleDateFormat;

/**
 * Created by ozden on 10/22/15.
 */
public class DateUtil {

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.");

    public static Date getSQLDate(java.util.Date date)
    {
        return new Date(date.getTime());
    }

    public static String getStringDate(Date date)
    {
        return sdf.format(new java.util.Date(date.getTime()));
    }

}
