package actors;

import actors.messages.*;
import akka.actor.UntypedActor;
import models.Question;
import services.QuestionService;
import services.serviceimpl.QuestionServiceImpl;

import java.util.List;

/**
 * Question Actor Class
 * @author ozden
 * Date: 22nd of October.
 * @version 1.0 MILESTONE 2
 *
 * The responsible actor to process all question messages regarding to
 * 1) Adding a question
 * 2) Getting last questions
 * 3) Searching messages.
 * 4) Getting all questions
 */
public class QuestionActor extends UntypedActor {
    @Override
    public void onReceive(Object message) throws Exception {

        if (message instanceof AddQuestionMessage)
        {
            QuestionService qs = new QuestionServiceImpl();
            qs.addQuestion(((AddQuestionMessage) message).q);

            getSender().tell("Adding question has been successful!", getSelf());
        }
        else if (message instanceof GetLastQuestionsMessage)
        {
            QuestionService qs = new QuestionServiceImpl();
            List<Question> questionList = qs.getLastQuestions(((GetLastQuestionsMessage) message).limit);

            getSender().tell(questionList, getSelf());
        }
        else if(message instanceof SearchMessage)
        {
            QuestionService qs = new QuestionServiceImpl();
            List<Question> questionList = qs.getSearchedQuestions(((SearchMessage) message).word);

            getSender().tell(questionList, getSelf());
        }
        else if (message instanceof GetAllQuestionsMessage)
        {
            QuestionService qs = new QuestionServiceImpl();
            List<Question> questionList = qs.getAllQuestions();

            getSender().tell(questionList, getSelf());
        }
        else if (message instanceof GetQuestionMessage)
        {
            QuestionService qs = new QuestionServiceImpl();
            Question q = qs.getQuestion(((GetQuestionMessage) message).questionId);

            getSender().tell(q, getSelf());
        }
    }
}
