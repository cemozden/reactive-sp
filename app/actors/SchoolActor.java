package actors;

import actors.messages.AddSchoolInformationMessage;
import actors.messages.GetSchoolInformationMessage;
import actors.messages.GetSchoolsByCityMessage;
import akka.actor.UntypedActor;
import services.SchoolService;
import services.serviceimpl.SchoolServiceImpl;

/**
 * City Actor Class
 * @author ozden
 * Date: 21st of October.
 * @version 1.0 MILESTONE 2
 *
 * The responsible actor to process all school messages regarding to
 * 1) Getting schools by city name
 * 2) Adding school information for a specific school.
 * 3) Getting school information of a specific school.
 */
public class SchoolActor extends UntypedActor {
    @Override
    public void onReceive(Object message) throws Exception
    {
        if (message instanceof GetSchoolsByCityMessage)
        {
            SchoolService ss = new SchoolServiceImpl();
            getSender().tell(ss.getSchoolsByCityName(((GetSchoolsByCityMessage) message).cityId), getSelf());
        }
        else if (message instanceof AddSchoolInformationMessage)
        {
            SchoolService sis = new SchoolServiceImpl();
            sis.addSchoolInformation(((AddSchoolInformationMessage) message).si);

            getSender().tell("School information has been added successfully!", getSelf());
        }
        else if (message instanceof GetSchoolInformationMessage)
        {
            SchoolService sis = new SchoolServiceImpl();

            getSender().tell(sis.getSchoolInformationList(((GetSchoolInformationMessage) message).schoolId), getSelf());
        }

    }
}
