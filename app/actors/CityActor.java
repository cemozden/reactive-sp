package actors;

import actors.messages.GetCitiesByCountryNameMessage;
import actors.messages.GetCityBySchoolIdMessage;
import akka.actor.UntypedActor;
import services.CityService;
import services.serviceimpl.CityServiceImpl;

/**
 * City Actor Class
 * @author ozden
 * Date: 19th of October.
 * @version 1.0 MILESTONE 2
 *
 * The responsible actor to process all city messages regarding to
 * 1) Getting the cities by a country
 * 2) Getting city by a school
 */
public class CityActor extends UntypedActor {

    @Override
    public void onReceive(Object message) throws Exception
    {
        if (message instanceof GetCitiesByCountryNameMessage)
        {
            CityService cs = new CityServiceImpl();
            getSender().tell(cs.getCitiesByCountryName(((GetCitiesByCountryNameMessage) message).countryId), getSelf());
        }
        else if(message instanceof GetCityBySchoolIdMessage)
        {
            CityService cs = new CityServiceImpl();
            getSender().tell(cs.getCityBySchoolId(((GetCityBySchoolIdMessage) message).schoolId), getSelf());
        }
    }
}
