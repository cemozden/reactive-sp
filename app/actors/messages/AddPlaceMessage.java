package actors.messages;

import models.Place;

import java.io.Serializable;

/**
 * AddPlaceMessage class
 * @author ozden
 * Date: 10/20/2015
 *
 * @version 1.0
 * Sent by Actors that's willing to add new places.
 */
public class AddPlaceMessage implements Serializable {

    public final Place place;

    public AddPlaceMessage(Place place) {
        this.place = place;
    }
}
