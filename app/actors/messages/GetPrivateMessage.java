package actors.messages;

import java.io.Serializable;

/**
 * GetPrivateMessage class
 * @author ozden
 * Date: 10/29/2015
 *
 * @version 1.0
 * Sent by Actors that's willing to a specific private message.
 */
public class GetPrivateMessage implements Serializable {

    public final String pMessageId;

    public GetPrivateMessage(String pMessageId) {
        this.pMessageId = pMessageId;
    }
}
