package actors.messages;

import models.Answer;

import java.io.Serializable;

/**
 * AddAnswerMessage class
 * @author ozden
 * Date: 10/21/2015
 *
 * @version 1.0
 * Sent by Actors that's willing to add new answers.
 */public class AddAnswerMessage implements Serializable {

    public final Answer answer;

    public AddAnswerMessage(Answer answer) {
        this.answer = answer;
    }
}
