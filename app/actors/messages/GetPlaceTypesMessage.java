package actors.messages;

import java.io.Serializable;

/**
 * GetPlaceTypesMessage class
 * @author ozden
 * Date: 10/21/2015
 *
 * @version 1.0
 * Sent by Actors that's willing to get place types.
 */
public class GetPlaceTypesMessage implements Serializable {}
