package actors.messages;

import models.Question;

import java.io.Serializable;

/**
 * AddQuestionMessage class
 * @author ozden
 * Date: 10/18/2015
 *
 * @version 1.0
 * Sent by Actors that's willing to add new questions.
 */
public class AddQuestionMessage implements Serializable {

    public final Question q;

    public AddQuestionMessage(Question q) {
        this.q = q;
    }
}
