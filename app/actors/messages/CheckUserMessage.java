package actors.messages;

import models.User;

/**
 * CheckUserMessage class
 * @author ozden
 * Date: 10/19/2015
 *
 * @version 1.0
 * Sent by Actors that's willing to check whether the user is already registered.
 */
public class CheckUserMessage {

    public final User user;

    public CheckUserMessage(User user) {
        this.user = user;
    }
}
