package actors.messages;

/**
 * Created by ozden on 11/22/15.
 */
public class GetUserMessage {

    public final String userId;

    public GetUserMessage(String userId) {
        this.userId = userId;
    }
}
