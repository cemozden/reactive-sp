package actors.messages;

import models.PlaceType;

import java.io.Serializable;

/**
 * AddPlaceTypeMessage class
 * @author ozden
 * Date: 10/20/2015
 *
 * @version 1.0
 * Sent by Actors that's willing to add new place types.
 */
public class AddPlaceTypeMessage implements Serializable {
    public final PlaceType pt;

    public AddPlaceTypeMessage(PlaceType pt) {
        this.pt = pt;
    }
}
