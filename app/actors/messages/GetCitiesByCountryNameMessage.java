package actors.messages;

import java.io.Serializable;

/**
 * AddPlaceMessage class
 * @author ozden
 * Date: 10/19/2015
 *
 * @version 1.0
 * Sent by Actors that's willing to get cities by the name of the country.
 */
public class GetCitiesByCountryNameMessage implements Serializable {

    public final String countryId;

    public GetCitiesByCountryNameMessage(String countryId) {
        this.countryId = countryId;
    }
}
