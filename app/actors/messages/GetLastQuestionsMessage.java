package actors.messages;

import java.io.Serializable;

/**
 * GetLasQuestionsMessage class
 * @author ozden
 * Date: 10/20/2015
 *
 * @version 1.0
 * Sent by Actors that's willing to get the last questions of the message.
 */

public class GetLastQuestionsMessage implements Serializable {

    public final int limit;

    public GetLastQuestionsMessage(int limit) {
        this.limit = limit;
    }
}
