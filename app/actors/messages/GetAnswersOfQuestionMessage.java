package actors.messages;

import java.io.Serializable;

/**
 * AddPlaceMessage class
 * @author ozden
 * Date: 10/28/2015
 *
 * @version 1.0
 * Sent by Actors that's willing to get answers of a specific question.
 */
public class GetAnswersOfQuestionMessage implements Serializable {

    public final String questionId;

    public GetAnswersOfQuestionMessage(String questionId) {
        this.questionId = questionId;
    }
}
