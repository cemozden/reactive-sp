package actors.messages;

import models.SchoolInformation;

import java.io.Serializable;

/**
 * AddSchoolInformationMessage class
 * @author ozden
 * Date: 10/23/2015
 *
 * @version 1.0
 * Sent by Actors that's willing to add new school information.
 */
public class AddSchoolInformationMessage implements Serializable {

    public final SchoolInformation si;

    public AddSchoolInformationMessage(SchoolInformation si) {
        this.si = si;
    }
}
