package actors.messages;

import java.io.Serializable;

/**
 * GetAllQuestionsMessage class
 * @author ozden
 * Date: 10/28/2015
 *
 * @version 1.0
 * Sent by Actors that's willing to get all questions.
 */
public class GetAllQuestionsMessage implements Serializable {}
