package actors.messages;

/**
 * GetSchoolsByCityMessage class
 * @author ozden
 * Date: 10/21/2015
 *
 * @version 1.0
 * Sent by Actors that's willing to get schools by the name of the city.
 */
public class GetSchoolsByCityMessage {

    public final String cityId;

    public GetSchoolsByCityMessage(String cityId) {
        this.cityId = cityId;
    }
}
