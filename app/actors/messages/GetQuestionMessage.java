package actors.messages;

import java.io.Serializable;

/**
 * GetQuestionMessage class
 * @author ozden
 * Date: 10/28/2015
 *
 * @version 1.0
 * Sent by Actors that's willing to get a question message.
 */
public class GetQuestionMessage implements Serializable {
    public final String questionId;

    public GetQuestionMessage(String questionId) {
        this.questionId = questionId;
    }
}
