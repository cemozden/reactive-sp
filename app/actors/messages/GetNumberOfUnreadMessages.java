package actors.messages;

/**
 * AddPlaceMessage class
 * @author ozden
 * Date: 10/26/2015
 *
 * @version 1.0
 * Sent by Actors that's willing to add new places.
 */
public class GetNumberOfUnreadMessages {

    public final String receiver;

    public GetNumberOfUnreadMessages(String receiver) {
        this.receiver = receiver;
    }
}
