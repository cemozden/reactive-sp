package actors.messages;

import java.io.Serializable;

/**
 * GetPrivateMessages class
 * @author ozden
 * Date: 10/29/2015
 *
 * @version 1.0
 * Sent by Actors that's willing to get a recivers' private messages.
 */
public class GetPrivateMessages implements Serializable {

    public final String receiver;

    public GetPrivateMessages(String receiver) {
        this.receiver = receiver;
    }
}
