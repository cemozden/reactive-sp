package actors.messages;

import models.User;

import java.io.Serializable;

/**
 * AddPlaceMessage class
 * @author ozden
 * Date: 10/17/2015
 *
 * @version 1.0
 * Sent by Actors that's willing to register new users.
 */
public class UserRegisterMessage implements Serializable {

    public final User user;

    public UserRegisterMessage(User user) {
        this.user = user;
    }
}
