package actors.messages;

/**
 * Created by ozden on 10/24/15.
 */
public class GetSchoolInformationMessage {

    public final String schoolId;

    public GetSchoolInformationMessage(String schoolId) {
        this.schoolId = schoolId;
    }
}
