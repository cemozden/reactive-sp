package actors.messages;

import java.io.Serializable;

/**
 * GetCityBySchoolIdMessage class
 * @author ozden
 * Date: 10/22/2015
 *
 * @version 1.0
 * Sent by Actors that's willing to get cities by the school id.
 */
public class GetCityBySchoolIdMessage implements Serializable {

    public final String schoolId;

    public GetCityBySchoolIdMessage(String schoolId) {
        this.schoolId = schoolId;
    }
}
