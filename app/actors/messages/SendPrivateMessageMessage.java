package actors.messages;

import models.PrivateMessage;

import java.io.Serializable;

/**
 * SendPrivateMessageMessage class
 * @author ozden
 * Date: 10/23/2015
 *
 * @version 1.0
 * Sent by Actors that's willing to send a new private message.
 */
public class SendPrivateMessageMessage implements Serializable {

    public final PrivateMessage pm;

    public SendPrivateMessageMessage(PrivateMessage pm) {
        this.pm = pm;
    }
}
