package actors.messages;

import java.io.Serializable;

/**
 * SearchMessage class
 * @author ozden
 * Date: 10/28/2015
 *
 * @version 1.0
 * Sent by Actors that's willing to search in the system.
 */
public class SearchMessage implements Serializable {

    public final String word;

    public SearchMessage(String word) {
        this.word = word;
    }
}
