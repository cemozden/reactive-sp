package actors.messages;

import java.io.Serializable;

/**
 * AddPlaceMessage class
 * @author ozden
 * Date: 10/17/2015
 *
 * @version 1.0
 * Sent by Actors that's willing to get countries.
 */
public class GetCountriesMessage implements Serializable {}