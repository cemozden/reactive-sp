package actors;

import actors.messages.AddPlaceMessage;
import actors.messages.AddPlaceTypeMessage;
import actors.messages.GetPlaceTypesMessage;
import actors.messages.SearchMessage;
import akka.actor.UntypedActor;
import models.Place;
import models.Search;
import services.PlaceService;
import services.serviceimpl.PlaceServiceImpl;

import java.util.List;

/**
 * City Actor Class
 * @author ozden
 * Date: 21st of October.
 * @version 1.0 MILESTONE 2
 *
 * The responsible actor to process all place messages regarding to
 * 1) Adding a place type for the places in the cities.
 * 2) Adding a place for a specific city.
 * 3) Getting place types.
 * 4) Searching places.
 */
public class PlaceActor extends UntypedActor {

    @Override
    public void onReceive(Object message) throws Exception
    {
        if (message instanceof AddPlaceTypeMessage)
        {
            PlaceService ps = new PlaceServiceImpl();
            ps.addPlaceType(((AddPlaceTypeMessage) message).pt);

            getSender().tell("Adding place type is successful!", getSelf());
        }
        else if (message instanceof AddPlaceMessage)
        {
            PlaceService ps = new PlaceServiceImpl();
            ps.addPlace(((AddPlaceMessage) message).place);

            getSender().tell("Adding place is successful!", getSelf());
        }
        else if(message instanceof GetPlaceTypesMessage)
        {
            PlaceService ps = new PlaceServiceImpl();
            getSender().tell(ps.getPlaceTypes(), getSelf());
        }
        else if (message instanceof SearchMessage)
        {
            PlaceService ps = new PlaceServiceImpl();
            List<Place> placeList = ps.getSearchedPlaces(((SearchMessage) message).word);

            getSender().tell(placeList, getSelf());
        }

    }
}
