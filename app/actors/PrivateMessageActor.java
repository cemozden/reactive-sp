package actors;

import actors.messages.GetNumberOfUnreadMessages;
import actors.messages.GetPrivateMessage;
import actors.messages.GetPrivateMessages;
import actors.messages.SendPrivateMessageMessage;
import akka.actor.UntypedActor;
import models.PrivateMessage;
import services.PrivateMessageService;
import services.serviceimpl.PrivateMessageServiceImpl;

import java.util.List;

/**
 * Private Message Actor Class
 * @author ozden
 * Date: 23rd of October.
 * @version 1.0 MILESTONE 2
 *
 * The responsible actor to process all private messages regarding to
 * 1) Sending a private message to a specific user.
 * 2) Getting number of unread messages for a specific user.
 * 3) Getting private messages of the receiver.
 * 4) Getting a specific private message of the receiver.
 */
public class PrivateMessageActor extends UntypedActor {
    @Override
    public void onReceive(Object message) throws Exception {

        if (message instanceof SendPrivateMessageMessage)
        {
            PrivateMessageService pms = new PrivateMessageServiceImpl();
            pms.sendPrivateMessage(((SendPrivateMessageMessage) message).pm);

            getSender().tell("Private message has been sent successfully!", getSelf());
        }
        else if (message instanceof GetNumberOfUnreadMessages)
        {
            PrivateMessageService pms = new PrivateMessageServiceImpl();
            int num = pms.getUnreadMessageCount(((GetNumberOfUnreadMessages) message).receiver);

            getSender().tell(num, getSelf());
        }
        else if (message instanceof GetPrivateMessages)
        {
            PrivateMessageService pms = new PrivateMessageServiceImpl();
            List<PrivateMessage> privateMessageList = pms.getPrivateMessages(((GetPrivateMessages) message).receiver);

            getSender().tell(privateMessageList, getSelf());
        }
        else if(message instanceof GetPrivateMessage)
        {
            PrivateMessageService pms = new PrivateMessageServiceImpl();

            getSender().tell(pms.getPrivateMessage(((GetPrivateMessage) message).pMessageId), getSelf());
        }

    }
}
