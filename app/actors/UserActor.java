package actors;

import actors.messages.*;
import akka.actor.UntypedActor;
import play.Logger;
import services.CountryService;
import services.UserService;
import services.serviceimpl.CountryServiceImpl;
import services.serviceimpl.UserServiceImpl;

/**
 * User Actor Class
 * @author ozden
 * Date: 17th of October.
 * @version 1.0 MILESTONE 2
 *
 * The responsible actor to process all user messages regarding to
 * 1) Registering a user.
 * 2) Getting countries..
 * 3) Checking whether a user is registered into the system.
 */
public class UserActor extends UntypedActor {


    @Override
    public void onReceive(Object message) throws Exception {

        if (message instanceof UserRegisterMessage)
        {
            UserService us = new UserServiceImpl();

            UserRegisterMessage urm = (UserRegisterMessage) message;
            us.registerUser(urm.user);

            getSender().tell("Registration is Completed!", getSelf());
        }
        else if (message instanceof GetCountriesMessage)
        {
            CountryService cs = new CountryServiceImpl();
            getSender().tell(cs.getCountries(), getSelf());
        }
        else if (message instanceof CheckUserMessage)
        {
            UserService us = new UserServiceImpl();

            getSender().tell(us.isUser(((CheckUserMessage) message).user), getSelf());
        }
        else if (message instanceof GetUserMessage)
        {
            UserService us = new UserServiceImpl();

            getSender().tell(us.getUser(((GetUserMessage) message).userId), getSelf());
        }
        else if (message instanceof UsersListMessage)
        {
            UserService us = new UserServiceImpl();

            getSender().tell(us.getUserList(), getSelf());
        }

    }
}
