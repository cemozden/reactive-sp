package actors;

import actors.messages.AddAnswerMessage;
import actors.messages.GetAnswersOfQuestionMessage;
import akka.actor.UntypedActor;
import models.Answer;
import services.AnswerService;
import services.serviceimpl.AnswerServiceImpl;

import java.util.List;

/**
 * Answer Actor Class
 * @author ozden
 * Date: 22nd of October.
 * @version 1.0 MILESTONE 2
 *
 * The responsible actor to process all messages regarding to answers of the questions asked.
 */
public class AnswerActor extends UntypedActor {
    @Override
    public void onReceive(Object message) throws Exception {

        if (message instanceof AddAnswerMessage)
        {
            AnswerService as = new AnswerServiceImpl();
            as.addAnswer(((AddAnswerMessage) message).answer);

            // Send "Adding answer is successful" to the sender of AddAnswerMessage.
            getSender().tell("Adding answer is successful!", getSelf());
        }

        /**
         * get the question id from the message.
         * */
        else if(message instanceof GetAnswersOfQuestionMessage)
        {
            AnswerService as = new AnswerServiceImpl();
            List<Answer> answerList = as.getAnswersOfQuestion(((GetAnswersOfQuestionMessage) message).questionId);

            // Send back a list of answers of the question to the sender of the message.
            getSender().tell(answerList, getSelf());
        }
    }
}
