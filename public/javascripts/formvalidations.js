$(document).ready(function(){
$('#registrationform').validate({

        rules: {
            username: {
                required : true,
                minlength : 2
            },

            password : {
                required : true,
                minlength : 8
            },

            firstname : "required",
            lastname : "required",
            countryname : "required",
            city : "required",
            schoolId : "required",
            email : "required"

        },
        messages: {
            username : {
                required : "User name cannot be empty!",
                minlength: "The length of User Name must be at least 2 character."
            },

            password : {
                required : "Password cannot be empty!",
                minlength : "The length of password must be at least 8 character."
            },

            firstname : "First Name cannot be empty!",
            lastname : "Last Name cannot be empty!",
            countryname : "The country name cannot be empty!",
            city : "The city name cannot be empty!",
            schoolId : "The school name cannot be empty!"
        }

    });

    $('#addplaceform').validate({

        rules : {
            name : "required",
            address : "required"
        },
        messages : {
            name : "Place name cannot be empty!",
            address : "The address cannot be empty!"
        }

        });

    $('#addplacetypeform').validate({

         rules : {
             type : "required"
         },

         messages : {
             type : "Place name cannot be empty!"
         }

         });

    $('#addquestionform').validate({

         rules : {
             title : "required",
             message : "required"
         },

         messages : {
             title : "The title of the question cannot be empty!",
             message : "The message cannot be empty!"
         }

         });

    $('#addsiform').validate({

         rules : {
             title : "required",
             information : "required"
         },

         messages : {
             title : "The title of the School Information cannot be empty!",
             information : "The information message cannot be empty!"
         }

         });

    $('#addanswerform').validate({

         rules : {
             message : "required"
         },

         messages : {
             message : "The answer message cannot be empty!"
         }

         });

    $('#privatemessageform').validate({

         rules : {
             title : "required",
             message : "required"
         },

         messages : {
             title : "The title of the private message cannot be empty!",
             message : "The message cannot be empty!"
         }

         });


});